const PRECACHE = 'precache-v1';
const RUNTIME = 'runtime';

const PRECACHE_URLS = [
    'index.html',
    './', // Alias for index.html
    'css/app.css',
    'css/game.css',
    'css/scoreboard.css',
    'js/game.js',
    'fonts/ARCADE.woff2',
    'img/skier_crash.png',
    'img/skier_down.png',
    'img/skier_right_down.png',
    'img/skier_left_down.png',
    'img/rock_1.png',
    'img/rock_2.png',
    'img/tree_1.png',
    'img/tree_cluster.png',
];

self.addEventListener('install', (event) => {
    event.waitUntil(
        caches.open(PRECACHE)
        .then((cache) => cache.addAll(PRECACHE_URLS))
        .then(self.skipWaiting())
    );
});

self.addEventListener('activate', (event) => {
    const currentCaches = [PRECACHE, RUNTIME];
    event.waitUntil(
        caches.keys().then((cacheNames) => {
            return cacheNames.filter(
                (cacheName) => !currentCaches.includes(cacheName)
            );
        }).then((cachesToDelete) => {
            return Promise.all(cachesToDelete.map((cacheToDelete) => {
                return caches.delete(cacheToDelete);
            }));
        }).then(() => self.clients.claim())
    );
});

self.addEventListener('fetch', (event) => {
    if (event.request.url.startsWith(self.location.origin) === false) {
        return;
    }

    event.respondWith(
        caches.match(event.request).then((cachedResponse) => {
            if (cachedResponse) {
                return cachedResponse;
            }

            return caches.open(RUNTIME).then((cache) => {
                return fetch(event.request).then((response) => {
                    return cache.put(
                        event.request,
                        response.clone()
                    ).then(() => {
                        return response;
                    });
                });
            });
        })
    );
});
