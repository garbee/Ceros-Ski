'use strict';

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js');
}

class HasAssets {
    constructor() {
        this.assets = new Map();
    }

    loadAssets() {
        const assetPromises = [];

        const loadImage = (asset, assetName) => {
            return new Promise((resolve, reject) => {
                const img = new Image();
                img.addEventListener('load', () => {
                    img.width /= 2;
                    img.height /= 2;
                    this.assets.set(assetName, img);
                    resolve();
                });
                img.addEventListener('error', () => {
                    reject(new Error(`Failed to load image: ${asset}`));
                });
                img.src = asset;
            });
        };

        this.assets.forEach((asset, assetName) => {
            assetPromises.push(loadImage(asset, assetName));
        });

        return Promise.all(assetPromises);
    }
}

class Skier extends HasAssets {
    constructor() {
        super();
        this.directions = Object.freeze({
            CRASHED: 'skierCrash',
            LEFT: 'skierLeft',
            LEFTDOWN: 'skierLeftDown',
            DOWN: 'skierDown',
            RIGHTDOWN: 'skierRightDown',
            RIGHT: 'skierRight',
            UP: 'skierJumping',
        });
        this.direction = this.directions.RIGHT;
        this.speed = 8;
        this.assets = new Map(Object.entries({
            'skierCrash': 'img/skier_crash.png',
            'skierLeft': 'img/skier_left.png',
            'skierLeftDown': 'img/skier_left_down.png',
            'skierDown': 'img/skier_down.png',
            'skierRightDown': 'img/skier_right_down.png',
            'skierRight': 'img/skier_right.png',
        }));
    }

    get direction() {
        return this._direction;
    }

    set direction(direction) {
        if (Object.values(this.directions).includes(direction) === false) {
            throw new Error('Invalid direction provided for skier.');
        }

        this._direction = direction;
    }

    get currentAsset() {
        return this.assets.get(this._direction);
    }

    reset() {
        this.direction = this.directions.RIGHT;
    }
}

class Obstacles extends HasAssets {
  constructor() {
    super();
    this.assets = new Map(
      Object.entries({
        'tree': 'img/tree_1.png',
        'treeCluster': 'img/tree_cluster.png',
        'rock1': 'img/rock_1.png',
        'rock2': 'img/rock_2.png',
      })
    );
  }

  get types() {
    return Array.from(this.assets.keys());
  }

  get randomType() {
    const keys = Array.from(this.assets.keys());
    const index = this.randomInt(0, keys.length - 1);
    return keys[index];
  }

  get random() {
      return this.assets.get(this.randomType);
  }

  randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}

class Controls {
    constructor(type = 'arrow', gamepadId = undefined) {
        this.types = Object.freeze({
            ARROW: 'arrow',
            WSAD: 'wsad',
            CONTROLLER: 'controller',
        });
        this.directions = Object.freeze({
            UP: 'up',
            DOWN: 'down',
            LEFT: 'left',
            RIGHT: 'right',
            PAUSE: 'pause',
        });
        if (Object.values(this.types).includes(type) === false) {
          throw new Error(`Controls doesn't understand type ${type}`);
        }
        this.type = type;
        if (type === this.types.CONTROLLER && gamepadId === undefined) {
            throw new Error(
                'A gamepad index must be provided to use a controller'
            );
        }
        this.gamepadId = gamepadId;
        this.lastGamepadPauseTimestamp = undefined;
    }

    get gamepad() {
        return Array.from(navigator.getGamepads()).filter((item) => {
            return item !== null;
        }).find(
            (gamepad) => gamepad.id === this.gamepadId
        );
    }

    /**
     * Buttons and axes are guarded since Firefox has an issue with
     * getting things right with an analog capable controller.
     *
     * TODO: Build a configuration page so users can edit the keymap.
     */
    get gamepadDirection() {
        if (this.type !== this.types.CONTROLLER) {
            throw new Error('No controller bound');
        }

        const gamepad = this.gamepad;
        const buttons = gamepad.buttons;
        const axes = gamepad.axes;

        if ((buttons[9] !== undefined && buttons[9].pressed === true)) {
            this.lastGamepadPauseTimestamp = gamepad.timestamp;
            return this.directions.PAUSE;
        }

        if (
            (buttons[12] !== undefined && buttons[12].pressed === true) ||
            (axes[1] !== undefined && axes[1] <= -0.5)
        ) {
          return this.directions.UP;
        }
        if (
            (buttons[13] !== undefined && buttons[13].pressed === true) ||
            (axes[1] !== undefined && axes[1] >= 0.5)) {
          return this.directions.DOWN;
        }
        if (
            (buttons[14] !== undefined && buttons[14].pressed === true) ||
            (axes[0] !== undefined && axes[0] <= -0.5)
         ) {
          return this.directions.LEFT;
        }
        if (
            (buttons[15] !== undefined && buttons[15].pressed === true) ||
            (axes[0] !== undefined && axes[0] >= 0.5)
         ) {
          return this.directions.RIGHT;
        }
    }

    get arrowKeys() {
        return Object.freeze({
            UP: 38,
            DOWN: 40,
            LEFT: 37,
            RIGHT: 39,
            PAUSE: 27,
        });
    }

    get wsadKeys() {
        return Object.freeze({
            UP: 87,
            DOWN: 83,
            LEFT: 65,
            RIGHT: 68,
            PAUSE: 27,
        });
    }

    get keys() {
        return this[`${this.type}Keys`];
    }

    /**
     * Rumble support is built for Chrome only.
     * Edge requires an Xbox controller only for any Gamepad access.
     * Firefox has some support but it is hit or miss.
     * Safari has no support for Gamepads at all.
     * The spec for this is still in flux, but Chrome has something consistent.
     *
     * If the navigator is capable of vibrating (such as Chrome on phones)
     * then we also trigger vibration on that in addition to any controllers
     * that may be in use. This provides feedback on mobile devices where
     * available.
     *
     * Ref: https://docs.google.com/document/d/1jPKzVRNzzU4dUsvLpSXm1VXPQZ8FP-0lKMT-R_p-s6g/edit
     * Ref: https://w3c.github.io/gamepad/extensions.html#gamepadhapticactuatortype-enum
     */
    rumble() {
        if ('vibrate' in navigator) {
            navigator.vibrate(500);
        }
        if (this.type !== this.types.CONTROLLER) {
            return;
        }
        const actuator = this.gamepad.vibrationActuator;
        if (actuator === undefined) {
            return;
        }
        if (actuator.type !== 'dual-rumble') {
            return;
        }

        actuator.playEffect('dual-rumble', {
            duration: 500,
            strongMagnitude: .5,
            weakMagnitude: .5,
        });
    }

    recognize(event) {
        if (event instanceof Event === false) {
            throw new Error('An event must be provided to recognize');
        }

        if (this.type === this.types.CONTROLLER) {
            return false;
        }

        return Object.values(this.keys).includes(event.which);
    }

    getDirection(event) {
        if (this.recognize(event) === false) {
            return undefined;
        }

        switch (event.which) {
          case this.arrowKeys.UP:
          case this.wsadKeys.UP:
            return this.directions.UP;
          case this.arrowKeys.DOWN:
          case this.wsadKeys.DOWN:
            return this.directions.DOWN;
          case this.arrowKeys.LEFT:
          case this.wsadKeys.LEFT:
            return this.directions.LEFT;
          case this.arrowKeys.RIGHT:
          case this.wsadKeys.RIGHT:
            return this.directions.RIGHT;
          case this.arrowKeys.PAUSE:
          case this.wsadKeys.PAUSE:
            return this.directions.PAUSE;
        }
    }

    pointerMoveHandler(event, game) {
        const diffX = event.movementX;
        const diffY = event.movementY;

        if (diffX > 2) {
            game.setSkierDirectionFromController(this.directions.RIGHT);
            return;
        }
        if (diffX < -2) {
            game.setSkierDirectionFromController(this.directions.LEFT);
            return;
        }
        if (diffY > 2) {
            game.setSkierDirectionFromController(this.directions.DOWN);
            return;
        }
        if (diffY < -2) {
            game.setSkierDirectionFromController(this.directions.UP);
            return;
        }
    }
}

class Game extends HTMLElement {
    constructor(options = {controlType: 'arrow', gamepadId: undefined}) {
        super();
        this.attachShadow({mode: 'open'});
        const style = document.createElement('link');
        style.rel = 'stylesheet';
        style.href = '/css/game.css';
        this.shadowRoot.appendChild(style);
        this.classes = Object.freeze({
            CONTAINER: 'game',
            SCOREBOARD: 'game-scoreboard',
            CANVAS: 'game-canvas',
            ENDOVERLAY: 'game-end-overlay',
            ENDOVERLAYACTIVE: 'game-end-overlay--active',
            ENDOVERLAYHEADER: 'game-end-overlay_header',
            STARTNEWTRIGGER: 'game-start-new',
            PAUSEOVERLAY: 'game-pause-overlay',
            PAUSEOVERLAYACTIVE: 'game-pause-overlay--active',
            PAUSEOVERLAYHEADER: 'game-pause-overlay_header',
            PAUSEOVERLAYRESUMEOPTIONS: 'game-pause-overlay_resume-options',
        });
        this.container = document.createElement('div');
        this.container.classList.add(this.classes.CONTAINER);
        this.shadowRoot.appendChild(this.container);
        this.scoreboard = new Scoreboard(this);
        this.scoreboard.classList.add(this.classes.SCOREBOARD);
        this.container.appendChild(this.scoreboard);
        this.canvas = document.createElement('canvas');
        this.canvas.classList.add(this.classes.CANVAS);
        this.container.appendChild(this.canvas);
        this.ctx = this.canvas.getContext('2d');
        this.endGameNode = document.createElement('div');
        this.endGameNode.classList.add(this.classes.ENDOVERLAY);
        this.endGameNode.innerHTML = `
            <span class="${this.classes.ENDOVERLAYHEADER}">Game Over</span>
            <p>You have run out of lives.</p>
            <p>This isn't the end though, start a new game!</p>
            <button class="${this.classes.STARTNEWTRIGGER}">New Game</button>
        `;
        this.newGameButton = this.endGameNode.querySelector(
            `.${this.classes.STARTNEWTRIGGER}`
        );
        this.newGameClickHandler = this.startGame.bind(this);
        this.container.appendChild(this.endGameNode);
        this.pauseNode = document.createElement('div');
        this.container.appendChild(this.pauseNode);
        this.pauseNode.classList.add(this.classes.PAUSEOVERLAY);
        this.pauseNode.innerHTML = `
            <span class="${this.classes.PAUSEOVERLAYHEADER}">Paused</span>
            <p>
                To resume the game you may:
            </p>
            <ul class="${this.classes.PAUSEOVERLAYRESUMEOPTIONS}">
                <li>Press the \`esc\` key.</li>
                <li>Press the \`Start\` button if
                    you're using a controller.</li>
                <li>Double tap or click the game area.</li>
            </ul>
            <p>
                Enjoy the rest of your trip down the mountain!
            </p>
        `;
        this.rAFId = undefined;
        this.paused = false;
        this.pauseStartTime = undefined;
        this.lastCollissionRect = undefined;
        this.skier = new Skier();
        this.controller = new Controls(
            options.controlType,
            options.gamepadId
        );
        this._boundKeyEventHandler = this.keyHandler.bind(this);
        this._boundGameLoop = this.gameloop.bind(this);
        this._boundBlurHandler = this.blurHandler.bind(this);
        this._boundPointerMoveHandler = this.pointerMoveHandler.bind(this);
        this._boundDoubleClickHandler = this.doubleClickHandler.bind(this);
        this.gameOver = false;
        this.skierMapX = 0;
        this.skierMapY = 0;
        this.gameHeight = 0;
        this.gameWidth = 0;
        this.screenWidth = Math.max(
            document.documentElement.clientWidth,
            window.innerWidth || 0
        );
        this.availableObstacles = new Obstacles();
        this.obstacles = [];
        this.canvas.setAttribute('tabindex', '0');
    }

    pointerMoveHandler(event) {
        if (document.activeElement !== this || event.pointerType === 'mouse') {
            return;
        }
        this.controller.pointerMoveHandler(event, this);
    }

    randomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    keyHandler(event) {
        const direction = this.controller.getDirection(event);
        if (direction === undefined) {
            return;
        }
        event.preventDefault();
        this.setSkierDirectionFromController(direction);
    }

    setSkierDirectionFromController(direction) {
        switch (direction) {
            case this.controller.directions.LEFT:
                this.skier.direction = this.skier.directions.LEFTDOWN;
                break;
            case this.controller.directions.RIGHT:
                this.skier.direction = this.skier.directions.RIGHTDOWN;
                break;
            case this.controller.directions.DOWN:
                this.skier.direction = this.skier.directions.DOWN;
                break;
            case this.controller.directions.PAUSE:
                this.pauseGame();
            break;
        }
    }

    blurHandler() {
        this.pauseGame(true);
    }

    doubleClickHandler() {
        this.pauseGame(!this.paused);
    }

    pauseGame(forced = false) {
        if (this.gameOver === true) {
            return;
        }
        const startTime = (
            this.controller.lastGamepadPauseTimestamp || Date.now()
        );
        if (this.pauseStartTime === startTime) {
            return;
        }
        this.paused = forced === true ? true : !this.paused;
        this.pauseStartTime = startTime;
        this.pauseNode.classList.toggle(
            this.classes.PAUSEOVERLAYACTIVE,
            this.paused
        );
    }

    calculateOpenPosition(minX, maxX, minY, maxY) {
        let x = undefined;
        let y = undefined;
        let collision = true;

        while (collision !== undefined) {
            x = this.randomInt(minX, maxX);
            y = this.randomInt(minY, maxY);
            collision = this.obstacles.find((obstacle) => {
                return x > (obstacle.x - 50) &&
                    x < (obstacle.x + 50) &&
                    y > (obstacle.y - 50) &&
                    y < (obstacle.y + 50);
            });
        }

        return {
            x,
            y,
        };
    }

    placeRandomObstacle(minX, maxX, minY, maxY) {
        const position = this.calculateOpenPosition(minX, maxX, minY, maxY);

        this.obstacles.push({
            type: this.availableObstacles.randomType,
            x: position.x,
            y: position.y,
        });
    }

    placeInitialObstacles() {
        const numberObstacles = Math.ceil(
            this.randomInt(5, 7) *
            (this.gameWidth / 800) *
            (this.gameHeight / 500)
        );

        let minX = -50;
        let maxX = this.gameWidth + 50;
        let minY = this.gameHeight / 2 + 100;
        let maxY = this.gameHeight + 50;

        for (let i = 0; i < numberObstacles; i++) {
            this.placeRandomObstacle(minX, maxX, minY, maxY);
        }

        this.obstacles = this.obstacles.sort((obstacle) => {
            let obstacleImage = this.availableObstacles.assets.get(
                obstacle.type
            );
            return obstacle.y + obstacleImage.height;
        });
    }

    clearCanvas() {
        this.ctx.clearRect(0, 0, this.gameWidth, this.gameHeight);
    }

    moveSkier() {
        switch (this.skier.direction) {
            case this.skier.directions.LEFTDOWN:
                this.skierMapX -= Math.round(this.skier.speed / 1.4142);
                this.skierMapY += Math.round(this.skier.speed / 1.4142);
                break;
            case this.skier.directions.DOWN:
                this.skierMapY += this.skier.speed;
                break;
            case this.skier.directions.RIGHTDOWN:
                this.skierMapX += Math.round(this.skier.speed / 1.4142);
                this.skierMapY += Math.round(this.skier.speed / 1.4142);
                break;
            default:
                return;
        }
        this.placeNewObstacle(this.skier.direction);
    }

    intersectRect(r1, r2) {
        return !(
            r2.left > r1.right ||
            r2.right < r1.left ||
            r2.top > r1.bottom ||
            r2.bottom < r1.top
        );
    }

    get isColliding() {
        const skierImage = this.skier.currentAsset;
        const skierRect = {
            left: this.skierMapX + this.gameWidth / 2,
            right: this.skierMapX + skierImage.width + this.gameWidth / 2,
            top: this.skierMapY + skierImage.height - 5 + this.gameHeight / 2,
            bottom: this.skierMapY + skierImage.height + this.gameHeight / 2,
        };

        return this.obstacles.find((obstacle) => {
            let obstacleImage = this.availableObstacles.assets.get(
                obstacle.type
            );
            let obstacleRect = {
                left: obstacle.x,
                right: obstacle.x + obstacleImage.width,
                top: obstacle.y + obstacleImage.height - 5,
                bottom: obstacle.y + obstacleImage.height,
            };

            return this.intersectRect(skierRect, obstacleRect);
        });
    }

    checkIfSkierHitObstacle() {
        const collision = this.isColliding;

        if (collision !== undefined) {
            this.skier.direction = this.skier.directions.CRASHED;
            if (this.lastCollisionRect !== collision) {
                this.scoreboard.crashes += 1;
                if (this.scoreboard.outOfLives) {
                    this.endGame();
                }
                this.controller.rumble();
                this.lastCollisionRect = collision;
            }
            return;
        }
        this.lastCollissionRect = undefined;
    }

    drawSkier() {
        const skierImage = this.skier.currentAsset;
        const x = (this.gameWidth - skierImage.width) / 2;
        const y = (this.gameHeight - skierImage.height) / 2;

        this.ctx.drawImage(
            skierImage,
            x,
            y,
            skierImage.width,
            skierImage.height
        );
    }

    drawObstacles() {
        const newObstacles = [];

        this.obstacles.forEach((obstacle) => {
            const obstacleImage = this.availableObstacles.assets.get(
                obstacle.type
            );
            const x = obstacle.x - this.skierMapX - obstacleImage.width / 2;
            const y = obstacle.y - this.skierMapY - obstacleImage.height / 2;

            if (
                x < -100 ||
                x > this.gameWidth + 50 ||
                y < -100 ||
                y > this.gameHeight + 50
            ) {
                return;
            }

            this.ctx.drawImage(
                obstacleImage,
                x,
                y,
                obstacleImage.width,
                obstacleImage.height
            );

            newObstacles.push(obstacle);
        });

        this.obstacles = newObstacles;
    }

    placeNewObstacle(direction) {
        let max = 13;
        if (this.screenWidth >= 768) {
            max = 8;
        }
        const shouldPlaceObstacle = this.randomInt(1, max);
        if (shouldPlaceObstacle !== max) {
            return;
        }

        const leftEdge = this.skierMapX;
        const rightEdge = this.skierMapX + this.gameWidth;
        const topEdge = this.skierMapY;
        const bottomEdge = this.skierMapY + this.gameHeight;

        switch (direction) {
            case this.skier.directions.LEFT:
                this.placeRandomObstacle(
                    leftEdge - 50, leftEdge, topEdge, bottomEdge
                );
                break;
            case this.skier.directions.LEFTDOWN:
                this.placeRandomObstacle(
                    leftEdge - 50, leftEdge, topEdge, bottomEdge
                );
                this.placeRandomObstacle(
                    leftEdge, rightEdge, bottomEdge, bottomEdge + 50
                );
                break;
            case this.skier.directions.DOWN:
                this.placeRandomObstacle(
                    leftEdge, rightEdge, bottomEdge, bottomEdge + 50
                );
                break;
            case this.skier.directions.RIGHTDOWN:
                this.placeRandomObstacle(
                    rightEdge, rightEdge + 50, topEdge, bottomEdge
                );
                this.placeRandomObstacle(
                    leftEdge, rightEdge, bottomEdge, bottomEdge + 50
                );
                break;
            case this.skier.directions.RIGHT:
                this.placeRandomObstacle(
                    rightEdge, rightEdge + 50, topEdge, bottomEdge
                );
                break;
            case this.skier.directions.UP:
                this.placeRandomObstacle(
                    leftEdge, rightEdge, topEdge - 50, topEdge
                );
                break;
        }
    }

    checkGamepadDirection() {
        if (this.controller.type === this.controller.types.CONTROLLER) {
          const direction = this.controller.gamepadDirection;
          this.setSkierDirectionFromController(direction);
        }
    }

    gameloop() {
        this.checkGamepadDirection();
        this.rAFId = requestAnimationFrame(this._boundGameLoop);
        if (this.paused === true) {
            return;
        }
        this.ctx.save();
        // Retina support
        this.ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
        this.clearCanvas();
        this.moveSkier();
        this.checkIfSkierHitObstacle();
        this.drawSkier();
        this.drawObstacles();
        this.ctx.restore();
        this.scoreboard.distanceTraveled = this.skierMapY;
    }

    async connectedCallback() {
        const rect = this.getBoundingClientRect();
        this.gameWidth = rect.width;
        this.gameHeight = rect.height;
        this.canvas.width = this.gameWidth * window.devicePixelRatio;
        this.canvas.height = this.gameHeight * window.devicePixelRatio;
        this.canvas.addEventListener('keydown', this._boundKeyEventHandler);
        this.canvas.addEventListener('blur', this._boundBlurHandler);
        this.canvas.addEventListener('dblclick', this._boundDoubleClickHandler);
        this.canvas.addEventListener(
            'pointermove',
            this._boundPointerMoveHandler
        );
        this.newGameButton.addEventListener('click', this.newGameClickHandler);
        await Promise.all([
            this.availableObstacles.loadAssets(),
            this.skier.loadAssets(),
        ]);
        this.startGame();
    }

    updateSettings(settings) {
        this.controller = new Controls(
            settings.controls,
            settings.controllerId
        );
        this.skier.speed = parseInt(settings.speed, 10);
        this.scoreboard.lifeCount = parseInt(settings.lives, 10);
        this.startGame();
    }

    startGame() {
        if (this.rAFId !== undefined) {
            cancelAnimationFrame(this.rAFId);
        }
        this.obstacles = [];
        this.skierMapX = 0;
        this.skierMapY = 0;
        this.gameOver = false;
        this.scoreboard.reset();
        this.skier.reset();
        this.clearCanvas();
        this.placeInitialObstacles();
        this.endGameNode.classList.remove(this.classes.ENDOVERLAYACTIVE);
        this.rAFId = requestAnimationFrame(this._boundGameLoop);
    }

    endGame() {
        cancelAnimationFrame(this.rAFId);
        this.gameOver = true;
        this.endGameNode.classList.add(this.classes.ENDOVERLAYACTIVE);
    }

    disconnectedCallback() {
        cancelAnimationFrame(this.rAFId);
        this.rAFId = undefined;
        this.canvas.removeEventListener('keydown', this._boundKeyEventHandler);
        this.canvas.removeEventListener('blur', this._boundPauseHandler);
        this.canvas.removeEventListener(
            'dblclick',
            this._boundDoubleClickHandler
        );
        this.canvas.removeEventListener(
            'pointermove',
            this._boundPointerMoveHandler
        );
    }
}

class Scoreboard extends HTMLElement {
    constructor(game = undefined) {
        super();
        this.attachShadow({mode: 'open'});
        this.classes = Object.freeze({
            CONTAINER: 'scoreboard',
            SCORECONTAINER: 'scoreboard-scores',
            SCOREHEADER: 'scoreboard-scores_header',
            LIFEVALUE: 'scoreboard-scores_life-value',
            DISTANCEVALUE: 'scoreboard-scores_distance-value',
            CRASHVALUE: 'scoreboard-scores_crash-value',
            CONTROLSCONTAINER: 'scoreboard-controls',
            SETTINGSBUTTON: 'scoreboard-controls_settings',
            SETTINGSDIALOG: 'scoreboard-settings-dialog',
            SETTINGSFORM: 'scoreboard-settings-form',
            SETTINGCONTROLS: 'scorebaord-settings-form_controls',
            SETTINGSCONTINUE: 'scoreboard-settings-continue',
        });
        this.game = game;
        this.shadowRoot.innerHTML = `
            <link rel="stylesheet" href="/css/scoreboard.css">
            <div class="${this.classes.CONTAINER}">
            <div class="${this.classes.SCORECONTAINER}">
                <div>
                    <span class="${this.classes.SCOREHEADER}">Lives</span>
                    <span class="${this.classes.LIFEVALUE}"></span>
                </div>
                <div>
                    <span class="${this.classes.SCOREHEADER}">Distance</span>
                    <span class="${this.classes.DISTANCEVALUE}"></span>
                </div>
                <div>
                    <span class="${this.classes.SCOREHEADER}">Crashes</span>
                    <span class="${this.classes.CRASHVALUE}">0</span>
                </div>
            </div>
            <div class="${this.classes.CONTROLSCONTAINER}">
                <button class="${this.classes.SETTINGSBUTTON}">Settings</button>
            </div>
            <dialog
                class="${this.classes.SETTINGSDIALOG}">
                <span>Settings</span>
                <hr>
                <details>
                  <summary>Control list</summary>
                <dl>
                  <dt>Up arrow (arrow keys)</dt>
                  <dt>W key (wsad keys)</dt>
                  <dd>Jump</dd>
                  <dt>Down Arrow (arrow keys)</dt>
                  <dt>S key (wsad keys)</dt>
                  <dd>Move down</dd>
                  <dt>Left arrow (arrow keys)</dt>
                  <dt>A key (wsad keys)</dt>
                  <dd>Move Left</dd>
                  <dt>Right arrow (arrow keys)</dt>
                  <dt>D key (wsad keys)</dt>
                  <dd>Move Right</dd>
                  <dt>Escape (esc) (all controls)</dt>
                  <dd>Pause game</dd>
                </dl>
                <p>
                  If you have a gamepad connected, you may also use that.
                  The direction pad and first control axis is mapped to the
                  directions.
                  Start buttons are mapped to pause the game.
                </p>
              </details>
              <form class="${this.classes.SETTINGSFORM}">
              <fieldset class="${this.classes.SETTINGCONTROLS}">
              <legend>Controls</legend>
                <label>
                <input type="radio" name="controls" value="arrow" checked>
                Arrow Keys</label>
                <label>
                <input type="radio" name="controls" value="wsad">
                WSAD Keys</label>
                </fieldset>
            <br>
            <input type=hidden name="lives" value="-1">
              <label>
                Life Limit
              <input type=checkbox name="lives" value="3">
              </label>
            <br>
            <label>
              Speed
              <br>
              <span>Slow</span>
              <input name="speed" type=range min="4" max="12" value="8">
              <span>Fast</span>
            </label>
            <div>
                <button type="submit">Start Game</button>
                <button type="reset">Reset options</button>
                <button type="button" class="${this.classes.SETTINGSCONTINUE}">
                    Continue current game
                </button>
            </div>
              </form>
            </dialog>
        </div>
        `;
        this.lifeNode = this.shadowRoot.querySelector(
            `.${this.classes.LIFEVALUE}`
        );
        this.distanceNode = this.shadowRoot.querySelector(
            `.${this.classes.DISTANCEVALUE}`
        );
        this.crashesNode = this.shadowRoot.querySelector(
            `.${this.classes.CRASHVALUE}`
        );
        this.settingsDialog = this.shadowRoot.querySelector(
            `.${this.classes.SETTINGSDIALOG}`
        );
        this.settingControlFieldset = this.shadowRoot.querySelector(
            `.${this.classes.SETTINGCONTROLS}`
        );
        this.settingsForm = this.shadowRoot.querySelector(
            `.${this.classes.SETTINGSFORM}`
        );
        this.settingsButton = this.shadowRoot.querySelector(
            `.${this.classes.SETTINGSBUTTON}`
        );
        this.continueButton = this.shadowRoot.querySelector(
            `.${this.classes.SETTINGSCONTINUE}`
        );
        this._boundSettingClickHandler = this.showSettings.bind(this);
        this._boundGamepadConnectedHandler = this.gamepadConnected.bind(this);
        this._boundGamepadDisconnectHandler =
            this.gamepadDisconnected.bind(this);
        this._boundSettingsSubmitHandler = this.settingsSubmit.bind(this);
        this._boundContinueHandler = this.continueHandler.bind(this);
        this._lifeCount = 0;
        this._distance = 0;
        this._crashes = 0;
        this.lifeCount = -1;
    }

    get outOfLives() {
        return this.lifeCount === -1 ? false : this._crashes >= this._lifeCount;
    }

    get crashes() {
        return this._crashes;
    }

    set crashes(crashes) {
        this._crashes = crashes;
        this.crashesNode.textContent = crashes;
    }

    get lifeCount() {
        return this._lifeCount;
    }

    set lifeCount(count) {
        this._lifeCount = count < 0 ? -1 : count;
        this.lifeNode.textContent = this._lifeCount === -1 ?
            'Limitless' :
            count;
    }

    get distanceTraveled() {
        return this._distance;
    }

    set distanceTraveled(distance) {
        this._distance = Math.floor(distance / 10);
        this.distanceNode.textContent = this._distance;
    }

    reset() {
        this.distanceTraveled = 0;
        this.crashes = 0;
    }

    connectedCallback() {
        this.settingsButton.addEventListener(
            'click',
            this._boundSettingClickHandler
        );
        this.settingsForm.addEventListener(
            'submit', this._boundSettingsSubmitHandler
        );
        this.continueButton.addEventListener(
            'click',
            this._boundContinueHandler
        );
        window.addEventListener(
            'gamepadconnected', this._boundGamepadConnectedHandler
        );
        window.addEventListener(
            'gamepaddisconnected', this._boundGamepadDisconnectHandler
        );
    }

    disconnectedCallback() {
        this.settingsButton.removeEventListener(
            'click',
            this._boundSettingClickHandler
        );
        this.settingsForm.removeEventListener(
            'submit', this._boundSettingsSubmitHandler
        );
        this.continueButton.removeEventListener(
            'click',
            this._boundContinueHandler
        );
        window.removeEventListener(
            'gamepadconnected', this._boundGamepadConnectedHandler
        );
        window.removeEventListener(
            'gamepaddisconnected', this._boundGamepadDisconnectHandler
        );
    }

    showSettings() {
        this.settingsDialog.showModal();
    }

    settingsSubmit(event) {
        event.preventDefault();
        if (this.game === undefined) {
            console.warn(
                'There is no game bound to provide setting updates to.'
            );
            return;
        }
        const form = event.target;
        const data = new Map(new FormData(form));
        const settings = {};
        Array.from(data.keys()).forEach((key) => {
            settings[key] = data.get(key);
        });
        const selectedControlMethod = Array.from(
            form.controls).find((element) => {
                return element.checked;
            });
        settings.controllerId = selectedControlMethod.dataset.id;

        this.game.updateSettings(settings);
        this.settingsDialog.close();
    }

    continueHandler() {
        this.settingsDialog.close();
    }

    makeGamepadOption(gamepad) {
        const label = document.createElement('label');
        const input = document.createElement('input');
        input.type = 'radio';
        input.name = 'controls';
        input.value = 'controller';
        input.dataset.id = gamepad.id;
        label.appendChild(input);
        const text = document.createTextNode(gamepad.id);
        label.appendChild(text);
        return label;
    }

    gamepadConnected(event) {
        const option = this.makeGamepadOption(event.gamepad);
        this.settingControlFieldset.appendChild(option);
    }

    gamepadDisconnected(event) {
        const id = event.gamepad.id;
        const input = Array.from(this.settingControlFieldset.elements).find(
            (element) => element.dataset.id === id
        );
        this.settingControlFieldset.removeChild(input.parentElement);
    }
}

customElements.define('app-scoreboard', Scoreboard);
customElements.define('app-game', Game);
