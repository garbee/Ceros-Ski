module.exports = {
    'extends': 'google',
    'parserOptions': {
        'ecmaVersion': 8,
    },
    'env': {
        'browser': true,
    },
    'rules': {
        'require-jsdoc': 0,
    }
};
